<div class="col-lg-2 col-md-3">
    <div class="cart_area">
        <div class="wishlist_link">
            <a href="#"><i class="ion-ios-heart-outline"></i></a>
        </div>
        <div class="cart_link">
            <a v-on:click="changePage('cart')"><i class="ion-ios-cart-outline"></i>My Cart</a>
            <span class="cart_count">@{{ countItemCart }}</span>
            <!--mini cart-->
            <div class="mini_cart">
                <div class="items_nunber">
                    <span>2 Items in Cart</span>
                </div>
                <div class="cart_button checkout">
                    <a href="checkout.html">Proceed to Checkout</a>
                </div>
                <div class="cart_item">
                    <div class="cart_img">
                        <a href="#"><img src="{{ asset('frontend/assets/img/cart/cart1.jpg') }}" alt=""></a>
                    </div>
                    <div class="cart_info">
                        <a href="#">Mr.Coffee 12-Cup</a>
                        <form action="#">
                            <input min="0" max="100" type="number">
                            <span>$60.00</span>
                        </form>
                    </div>
                </div>
                <div class="cart_item">
                    <div class="cart_img">
                        <a href="#"><img src="{{ asset('frontend/assets/img/cart/cart2.jpg') }}" alt=""></a>
                    </div>
                    <div class="cart_info">
                        <a href="#">Lid Cover Cookware</a>
                        <form action="#">
                            <input min="0" max="100" type="number">
                            <span>$160.00</span>
                        </form>
                    </div>
                </div>
                <div class="cart_button view_cart">
                    <a href="#">View and Edit Cart</a>
                </div>
            </div>
            <!--mini cart end-->
        </div>
    </div>
</div>