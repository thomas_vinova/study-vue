<div class="shop_wrapper shop_fullwidth">
    <div class="container">
            <!--shop toolbar start-->
        <div class="row">
            <div class="col-12">
                <div class="shop_toolbar">

                    <div class="list_button">
                        <ul class="nav" role="tablist">
                            <li>
                                <a class="active" data-toggle="tab" href="#large" role="tab" aria-controls="large" aria-selected="true"><i class="ion-grid"></i></a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#list" role="tab" aria-controls="list" aria-selected="false"><i class="ion-android-menu"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="select_option number">
                        <form action="#">
                            <label>Show:</label>
                            <select name="orderby" id="short">
                                <option selected value="1">9</option>
                                <option value="1">19</option>
                                <option value="1">30</option>
                            </select>
                        </form>
                    </div>
                    <div class="select_option">
                        <form action="#">
                            <label>Sort By</label>
                            <select name="orderby" id="short1">
                                <option selected value="1">Position</option>
                                <option value="1">Price: Lowest</option>
                                <option value="1">Price: Highest</option>
                                <option value="1">Product Name:Z</option>
                                <option value="1">Sort by price:low</option>
                                <option value="1">Product Name: Z</option>
                                <option value="1">In stock</option>
                                <option value="1">Product Name: A</option>
                                <option value="1">In stock</option>
                            </select>
                        </form>
                    </div>
                </div>
            </div>
        </div>        
        <!--shop toolbar end-->
                
            <!--shop tab product-->
        <div class="shop_tab_product">   
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="large" role="tabpanel">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-6" v-for='(item,key) in dataProduct'>
                            <div class="single_product"> 
                                <div class="product_thumb">
                                        <a href=""><img v-bind:src="'frontend/img/product/' + item.image" alt=""></a>
                                        <div class="btn_quickview">
                                            <a href="#" data-toggle="modal" data-target="#modal_box"  title="Quick View"><i class="ion-ios-eye"></i></a>
                                    </div>
                                </div> 
                                <div class="product_content">   
                                    <div class="product_ratting">
                                        <ul>
                                            <li><a href="#"><i class="ion-star"></i></a></li>
                                            <li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
                                            <li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
                                            <li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
                                            <li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
                                        </ul>
                                    </div>
                                    <h3><a href="">@{{ item.name }}</a></h3>
                                    <div class="product_price">
                                        <span class="current_price">@{{ item.price | formatPrice }}</span>
                                    </div>
                                    <div class="product_action">
                                        <ul>
                                            <li class="product_cart"><a v-on:click="addCart(key)" title="Add to Cart">Add to Cart</a></li>
                                            <li class="add_links"><a href="#" title="Add to Wishlist"><i class="ion-ios-heart-outline"></i></a></li>
                                            <li class="add_links"><a href="#" title="Add to Compare"><i class="ion-loop"></i></a></li>
                                        </ul>
                                    </div>
                                </div>    
                            </div>
                        </div>
                    </div>     
                </div>

            </div>
        </div>    
        <!--shop tab product end-->
        
        <!--pagination style start--> 
        <div class="row">
            <div class="col-12">  
                <div class="pagination_style fullwidth">
                    <ul>
                        <li class="current_number">1</li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                </div>
            </div>      
        </div>
        <!--pagination style end-->    
    </div>
</div>