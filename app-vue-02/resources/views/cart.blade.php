<div class="shopping_cart_area">
    <div class="container">
        <form action="#">
            <div class="row">
                <div class="col-12">
                    <div class="table_desc">
                        <div class="cart_page table-responsive">
                            <table>
                                <thead>
                                    <tr>
                                    <th class="product_remove">Delete @{{ totalItemCart }}</th>
                                        <th class="product_thumb">Image</th>
                                        <th class="product_name">Product</th>
                                        <th class="product-price">Price</th>
                                        <th class="product_quantity">Quantity</th>
                                        <th class="product_total">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-if='isEmpty'>
                                        <td colspan="6">No Product In Cart</td>
                                    </tr>
                                    <tr v-else v-for='(item,index) in dataCart'>
                                        <td class="product_remove" v-if="index % 2" style="background: #c1c1c1"><a v-on:click="deleteItem(index)"><i class="fa fa-trash-o"></i></a></td>
                                        <td class="product_remove" v-else style="background: white"><a v-on:click="deleteItem(index)"><i class="fa fa-trash-o"></i></a></td>

                                        <td class="product_thumb" v-if="index % 2" style="background: #c1c1c1"><a href="#"><img v-bind:src="'frontend/img/product/' + item.image" alt=""></a></td>
                                        <td class="product_thumb" v-else style="background: white"><a href="#"><img v-bind:src="'frontend/img/product/' + item.image" alt=""></a></td>

                                        <td class="product_name" v-if="index % 2" style="background: #c1c1c1"><a href="#">@{{ item.name }}</a></td>
                                        <td class="product_name" v-else style="background: white"><a href="#">@{{ item.name }}</a></td>

                                        <td class="product-price" v-if="index % 2" style="background: #c1c1c1">@{{ item.price | formatPrice }}</td>
                                        <td class="product-price" v-else style="background: white">@{{ item.price | formatPrice }}</td>

                                        <td class="product_quantity" v-if="index % 2" style="background: #c1c1c1"><input min="0" max="100" v-bind:value="item.qty" type="number"></td>
                                        <td class="product_quantity" v-else style="background: white"><input min="0" max="100" v-bind:value="item.qty" type="number"></td>

                                        <td class="product_total" v-if="index % 2" style="background: #c1c1c1">@{{ item.price * item.qty | formatPrice }}</td>
                                        <td class="product_total"v-else style="background: white">@{{ item.price * item.qty | formatPrice }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="cart_submit">
                            <button type="submit">update cart</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--coupon code area start-->
            <div class="coupon_area">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="coupon_code left">
                            <h3>Coupon</h3>
                            <div class="coupon_inner">
                                <p>Enter your coupon code if you have one.</p>
                                <input placeholder="Coupon code" type="text">
                                <button type="submit">Apply coupon</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="coupon_code right">
                            <h3>Cart Totals</h3>
                            <div class="coupon_inner">
                                <div class="cart_subtotal">
                                    <p>Total</p>
                                    <p class="cart_amount">@{{ totalPaymentcart | formatPrice }}</p>
                                </div>
                                <div class="checkout_btn">
                                    <a href="#">Proceed to Checkout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--coupon code area end-->
        </form>
    </div>
</div>