module.exports = 
[
    {
        "id": 0,
        "image": "bigimg1.jpg",
        "name": "Unbranded Soft Shirt",
        "price": "189000",
        "qty": 3
    },
    {
        "id": 1,
        "image": "bigimg2.jpg",
        "name": "Rustic Plastic Pants",
        "price": "768000",
        "qty": 3
    },
    {
        "id": 2,
        "image": "bigimg3.jpg",
        "name": "Small Rubber Fish",
        "price": "599000",
        "qty": 2
    },
    {
        "id": 3,
        "image": "bigimg4.jpg",
        "name": "Rustic Soft Hat",
        "price": "132000",
        "qty": 2
    },
    {
        "id": 4,
        "image": "product1.jpg",
        "name": "Generic Cotton Fish",
        "price": "16000",
        "qty": 0
    },
    {
        "id": 5,
        "image": "product10.jpg",
        "name": "Gorgeous Rubber Chips",
        "price": "539000",
        "qty": 0
    },
    {
        "id": 6,
        "image": "product11.jpg",
        "name": "Incredible Cotton Ball",
        "price": "403000",
        "qty": 0
    },
    {
        "id": 7,
        "image": "product12.jpg",
        "name": "Licensed Metal Table",
        "price": "523000",
        "qty": 6
    },
    {
        "id": 8,
        "image": "product13.jpg",
        "name": "Sleek Cotton Mouse",
        "price": "465000",
        "qty": 9
    },
    {
        "id": 9,
        "image": "product14.jpg",
        "name": "Rustic Cotton Gloves",
        "price": "387000",
        "qty": 3
    },
    {
        "id": 10,
        "image": "product15.jpg",
        "name": "Handcrafted Concrete Towels",
        "price": "175000",
        "qty": 1
    },
    {
        "id": 11,
        "image": "product16.jpg",
        "name": "Fantastic Soft Table",
        "price": "252000",
        "qty": 7
    },
    {
        "id": 12,
        "image": "product17.jpg",
        "name": "Incredible Frozen Soap",
        "price": "629000",
        "qty": 8
    },
    {
        "id": 13,
        "image": "product18.jpg",
        "name": "Ergonomic Concrete Ball",
        "price": "937000",
        "qty": 5
    },
    {
        "id": 14,
        "image": "product19.jpg",
        "name": "Refined Steel Pants",
        "price": "969000",
        "qty": 5
    },
    {
        "id": 15,
        "image": "product2.jpg",
        "name": "Practical Wooden Chicken",
        "price": "263000",
        "qty": 2
    },
    {
        "id": 16,
        "image": "product20.jpg",
        "name": "Rustic Steel Chair",
        "price": "694000",
        "qty": 9
    },
    {
        "id": 17,
        "image": "product21.jpg",
        "name": "Refined Wooden Keyboard",
        "price": "619000",
        "qty": 9
    },
    {
        "id": 18,
        "image": "product22.jpg",
        "name": "Practical Concrete Table",
        "price": "893000",
        "qty": 8
    },
    {
        "id": 19,
        "image": "product23.jpg",
        "name": "Handcrafted Soft Tuna",
        "price": "784000",
        "qty": 3
    },
    {
        "id": 20,
        "image": "product24.jpg",
        "name": "Generic Cotton Table",
        "price": "437000",
        "qty": 4
    },
    {
        "id": 21,
        "image": "product25.jpg",
        "name": "Rustic Fresh Gloves",
        "price": "741000",
        "qty": 3
    },
    {
        "id": 22,
        "image": "product26.jpg",
        "name": "Awesome Steel Shoes",
        "price": "505000",
        "qty": 9
    },
    {
        "id": 23,
        "image": "product27.jpg",
        "name": "Handcrafted Steel Cheese",
        "price": "622000",
        "qty": 6
    },
    {
        "id": 24,
        "image": "product28.jpg",
        "name": "Fantastic Wooden Chicken",
        "price": "456000",
        "qty": 9
    },
    {
        "id": 25,
        "image": "product29.jpg",
        "name": "Sleek Granite Computer",
        "price": "976000",
        "qty": 0
    },
    {
        "id": 26,
        "image": "product3.jpg",
        "name": "Licensed Concrete Cheese",
        "price": "760000",
        "qty": 6
    },
    {
        "id": 27,
        "image": "product30.jpg",
        "name": "Gorgeous Wooden Computer",
        "price": "252000",
        "qty": 5
    },
    {
        "id": 28,
        "image": "product31.jpg",
        "name": "Tasty Plastic Mouse",
        "price": "134000",
        "qty": 5
    },
    {
        "id": 29,
        "image": "product32.jpg",
        "name": "Small Soft Bike",
        "price": "855000",
        "qty": 8
    },
    {
        "id": 30,
        "image": "product33.jpg",
        "name": "Awesome Metal Tuna",
        "price": "483000",
        "qty": 0
    },
    {
        "id": 31,
        "image": "product34.jpg",
        "name": "Handmade Plastic Hat",
        "price": "880000",
        "qty": 9
    },
    {
        "id": 32,
        "image": "product35.jpg",
        "name": "Gorgeous Frozen Shoes",
        "price": "943000",
        "qty": 7
    },
    {
        "id": 33,
        "image": "product36.jpg",
        "name": "Awesome Granite Chicken",
        "price": "481000",
        "qty": 3
    },
    {
        "id": 34,
        "image": "product37.jpg",
        "name": "Awesome Cotton Sausages",
        "price": "412000",
        "qty": 4
    },
    {
        "id": 35,
        "image": "product38.jpg",
        "name": "Rustic Concrete Ball",
        "price": "597000",
        "qty": 2
    },
    {
        "id": 36,
        "image": "product39.jpg",
        "name": "Awesome Plastic Computer",
        "price": "433000",
        "qty": 1
    },
    {
        "id": 37,
        "image": "product4.jpg",
        "name": "Intelligent Plastic Chicken",
        "price": "845000",
        "qty": 2
    },
    {
        "id": 38,
        "image": "product40.jpg",
        "name": "Incredible Frozen Gloves",
        "price": "453000",
        "qty": 3
    },
    {
        "id": 39,
        "image": "product41.jpg",
        "name": "Small Wooden Salad",
        "price": "794000",
        "qty": 7
    },
    {
        "id": 40,
        "image": "product42.jpg",
        "name": "Sleek Wooden Chair",
        "price": "1000",
        "qty": 9
    },
    {
        "id": 41,
        "image": "product44.jpg",
        "name": "Incredible Frozen Ball",
        "price": "791000",
        "qty": 6
    },
    {
        "id": 42,
        "image": "product45.jpg",
        "name": "Gorgeous Frozen Towels",
        "price": "329000",
        "qty": 7
    },
    {
        "id": 43,
        "image": "product46.jpg",
        "name": "Licensed Plastic Chips",
        "price": "642000",
        "qty": 3
    },
    {
        "id": 44,
        "image": "product47.jpg",
        "name": "Generic Plastic Soap",
        "price": "601000",
        "qty": 7
    },
    {
        "id": 45,
        "image": "product48.jpg",
        "name": "Unbranded Plastic Shirt",
        "price": "421000",
        "qty": 3
    },
    {
        "id": 46,
        "image": "product49.jpg",
        "name": "Unbranded Wooden Bike",
        "price": "918000",
        "qty": 0
    },
    {
        "id": 47,
        "image": "product5.jpg",
        "name": "Refined Fresh Ball",
        "price": "485000",
        "qty": 7
    },
    {
        "id": 48,
        "image": "product50.jpg",
        "name": "Refined Metal Fish",
        "price": "670000",
        "qty": 5
    },
    {
        "id": 49,
        "image": "product51.jpg",
        "name": "Ergonomic Rubber Salad",
        "price": "530000",
        "qty": 6
    },
    {
        "id": 50,
        "image": "product52.jpg",
        "name": "Sleek Rubber Sausages",
        "price": "754000",
        "qty": 2
    },
    {
        "id": 51,
        "image": "product53.jpg",
        "name": "Handcrafted Rubber Chips",
        "price": "75000",
        "qty": 2
    },
    {
        "id": 52,
        "image": "product54.jpg",
        "name": "Generic Frozen Keyboard",
        "price": "625000",
        "qty": 5
    },
    {
        "id": 53,
        "image": "product55.jpg",
        "name": "Handmade Soft Soap",
        "price": "913000",
        "qty": 9
    },
    {
        "id": 54,
        "image": "product56.jpg",
        "name": "Handmade Soft Pants",
        "price": "38000",
        "qty": 8
    },
    {
        "id": 55,
        "image": "product57.jpg",
        "name": "Intelligent Metal Soap",
        "price": "18000",
        "qty": 1
    },
    {
        "id": 56,
        "image": "product58.jpg",
        "name": "Small Plastic Chicken",
        "price": "537000",
        "qty": 5
    },
    {
        "id": 57,
        "image": "product59.jpg",
        "name": "Awesome Granite Gloves",
        "price": "332000",
        "qty": 1
    },
    {
        "id": 58,
        "image": "product6.jpg",
        "name": "Handcrafted Granite Cheese",
        "price": "639000",
        "qty": 3
    },
    {
        "id": 59,
        "image": "product60.jpg",
        "name": "Sleek Wooden Cheese",
        "price": "351000",
        "qty": 0
    },
    {
        "id": 60,
        "image": "product61.jpg",
        "name": "Licensed Soft Fish",
        "price": "715000",
        "qty": 7
    },
    {
        "id": 61,
        "image": "product62.jpg",
        "name": "Handcrafted Wooden Tuna",
        "price": "210000",
        "qty": 4
    },
    {
        "id": 62,
        "image": "product63.jpg",
        "name": "Unbranded Concrete Bacon",
        "price": "795000",
        "qty": 5
    },
    {
        "id": 63,
        "image": "product64.jpg",
        "name": "Sleek Wooden Shoes",
        "price": "748000",
        "qty": 4
    },
    {
        "id": 64,
        "image": "product65.jpg",
        "name": "Licensed Metal Towels",
        "price": "345000",
        "qty": 3
    }
]