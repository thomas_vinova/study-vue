import Vue from 'vue';
import _ from 'lodash';

//import dataCart from './data.json';

/** Cách Gọi 1 */
//import dataCart from './data01';

/** Cách Gọi 2 */
var product = require('./data02');

let app = new Vue({
    el: '#app',

    data: {
        checkCartEmpty: null,
        dataCart: [],
        dataProduct : product,
        totalItemCart: null,
        
        pageCurrent: 'product',
    },

    computed: {
        isEmpty() {
            if (_.size(this.dataCart) > 0) { 
                return false;
            } else { 
                return true;
            }
        },

        countItemCart () {
            return _.size(this.dataCart);
        },

        totalPaymentcart () {
            var totalPayment = 0;
            if (_.size(this.dataCart) > 0) {
                for (let i = 0 ; i < _.size(this.dataCart) ; i++ ) {
                    totalPayment += this.dataCart[i].price * this.dataCart[i].qty;
                }
            }
            return totalPayment;
        }
    },

    filters: {
        formatPrice (price) {
            return price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + ' VND';
        }
    },

    methods: {
        addCart (id) {
            let item = this.dataProduct[id];

            if (_.some(this.dataCart, item)) {
                this.dataCart[item.id].qty++;
                this.changePage('cart');
            } else {
                item.price = parseInt(this.dataProduct[id].price,10);
                this.dataCart.push(item);
                this.changePage('cart');
            }
            
            
            return false;
            
        },
        changePage (page) {
            this.pageCurrent = page;
            return this.pageCurrent;
        },
        
        deleteItem (id) {
            this.dataCart.splice(id, 1); 
        }
    }

});