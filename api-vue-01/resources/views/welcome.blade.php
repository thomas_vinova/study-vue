<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style type="text/css">
        .bg-red {
            background:red;
        }
        .bg-blue {
            background:blue;
        }
        .text-red {
            color:red;
        }
        .text-blue {
            color:blue;
        }
    </style>
</head>
<body>
    <div id="root">
        <div id='home'>
            <img v-bind:src='image' v-bind:width='width' />

            <div>@{{ message }}</div>

            <ul v-bind:class='class1'>
                <li v-for='(value,key) in listToDo'>@{{ value }} - @{{ key }}</li>
            </ul>

            <ul v-bind:class='["text-blue","bg-red"]'>
                <li v-for='(value,key) in listDish'>@{{ value }} - @{{ key }}</li>
            </ul>

            <ul v-bind:class='class2'>
                <li v-for='(value,key) in listDish'>@{{ value }} - @{{ key }}</li>
            </ul>

            <ul v-bind:style='style1'>
                <li v-for='(value,key) in listDish'>@{{ value }} - @{{ key }}</li>
            </ul>

            <ul>
                <li v-bind:class='key % 2 == 0 ? odd : even' v-for='(value,key) in listToDo'>1 @{{ value }} - @{{ key }}</li>
            </ul>

            <ul>
                <li v-for='(value,key) in listToDo'>
                    <span v-if='key % 2 == 0' v-bind:class='odd'> @{{ value }} - @{{ key }}</span>
                    <span v-else v-bind:class='even'> @{{ value }} - @{{ key }}</span>
                </li>

            </ul>
        </div>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>