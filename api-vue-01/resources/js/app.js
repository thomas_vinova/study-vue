import Vue from "vue";

let app = new Vue({
    el: '#root',

    data: {
        message: 'Hello World You',
        image: 'https://target.scene7.com/is/image/Target/GUEST_82bc5831-9970-4051-ab45-87a09dfe68d7',
        width: '100px',
        listToDo: ['A','B','C','D','E','F'],
        itemCheck: 'A',
        listDish: {
            'mon1' : 'Com',
            'mon2' : 'Pho',
            'mon3' : 'Chao'
        },
        class1: ['text-red','bg-blue'],
        class2: {
            'text-red' : true,
            'bg-blue' : false
        },
        style1: {
            'color' : 'purple',
            'background' : 'pink'
        },
        odd: {
            'text-red' : true,
            'bg-blue' : true
        },
        even: {
            'text-blue' : true,
            'bg-red' : true
        }
    }
});