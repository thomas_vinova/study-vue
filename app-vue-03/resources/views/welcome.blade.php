<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<div id="root">
    <input type="button" value="Click" v-on:click="hello('Tuan')">

    <hr />

    <input type="button" value="Plus" v-on:click="countPlus()"> @{{ counter }}

    <hr />
    
    {{-- https://vuejs.org/v2/guide/events.html#Key-Modifiers --}}
    <input type="text" value="Text" v-on:keyup="countPlus()" />

    <hr />

    <input type="text" v-model="content" />
    @{{ content }}

    <hr />

    @{{ tiente | format_curency }}

    <hr />

    @{{ total | format_curency }}
</div>

<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>