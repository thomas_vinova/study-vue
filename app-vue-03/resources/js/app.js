import Vue from 'vue';
import _ from 'lodash';

var cart = require('./data01');

new Vue({
    el: '#root',

    data: {
        counter : 0,
        content : null,
        tiente : 1000000,
        cartItem: cart, 
    },

    filters: {
        format_curency(a) {
            return a.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
        }
    },

    computed: {
        total () {
            return _.sumBy(this.cartItem,'price')
        }
    },

    methods: {
        hello (name) {
            alert('Hello Ban ' + name)    
        },
        countPlus () {
            this.counter = this.counter + 1
        }
    },
});