module.exports =
[
    {
        "id": 0,
        "image": "bigimg1.jpg",
        "name": "Unbranded Soft Shirt",
        "price": "189000",
        "qty": 3
    },
    {
        "id": 1,
        "image": "bigimg2.jpg",
        "name": "Rustic Plastic Pants",
        "price": "768000",
        "qty": 3
    },
    {
        "id": 2,
        "image": "bigimg3.jpg",
        "name": "Small Rubber Fish",
        "price": "599000",
        "qty": 2
    }
]